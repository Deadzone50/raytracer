PRODUCT := prog

CXX := g++
LINKER := g++
INCDIRS := -I.
CXXFLAGS := -g -fopenmp -std=c++11 -Wall
LIBS := -fopenmp -lGL -lGLU -lglut

SRCFILES := $(wildcard *.cc)
OBJFILES := $(patsubst %.cc,%.o,$(SRCFILES))
DEPFILES := $(patsubst %.cc,%.d,$(SRCFILES))

$(PRODUCT): $(OBJFILES)
	$(LINKER) $^ -o $@ $(LIBS)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(INCDIRS) -c $< -o $@

%.d: %.cpp
	$(CXX) $(INCDIRS) -MM $< > $@
	
clean:
	$(RM) *.o *~ $(PRODUCT)

-include $(DEPFILES)
