#include "Drawing.hh"
#include <GL/glut.h>

void DrawPoint(Vec2i Screen, int x, int y, Vec3f color)
{
	float OffsetX = (float)Screen.x/2;
	float OffsetY = (float)Screen.y/2;
	float X = (x-OffsetX)/(OffsetX);
	float Y = (y-OffsetY)/(OffsetY);
	
	glColor3f(color.x, color.y, color.z);
	glBegin(GL_POINTS);
	{           
        glVertex2f(X, Y);
    }
    glEnd();
}

void DrawLine(Vec2i Screen, int x1, int y1, int x2, int y2, Vec3f color)
{
	float OffsetX = (float)Screen.x/2;
	float OffsetY = (float)Screen.y/2;
	float X1 = (x1-OffsetX)/(OffsetX);
	float Y1 = (y1-OffsetY)/(OffsetY);
	float X2 = (x2-OffsetY)/(OffsetX);
	float Y2 = (y2-OffsetY)/(OffsetY);
	
	glColor3f(color.x, color.y, color.z);
	glBegin(GL_LINES);
	{           
        glVertex2f(X1, Y1);
        glVertex2f(X2, Y2);
    }
    glEnd();
}

void DrawRect(Vec2i Screen, int x1, int y1, int x2, int y2, Vec3f color)
{
	float OffsetX = (float)Screen.x/2;
	float OffsetY = (float)Screen.y/2;
	float X1 = (x1-OffsetX)/(OffsetX);
	float Y1 = (y1-OffsetY)/(OffsetY);
	float X2 = (x2-OffsetY)/(OffsetX);
	float Y2 = (y2-OffsetY)/(OffsetY);
	
	glColor3f(color.x, color.y, color.z);
	glBegin(GL_QUADS);
	{           
        glVertex2f(X1, Y1);
        glVertex2f(X2, Y1);
        glVertex2f(X2, Y2);
        glVertex2f(X1, Y2);
    }
    glEnd();
}
