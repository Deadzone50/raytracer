#ifndef DRAWING_HH
#define DRAWING_HH

#include "Vector.hh"

void DrawPoint(Vec2i Screen, int x, int y, Vec3f color);

void DrawLine(Vec2i Screen, int x1, int y1, int x2, int y2, Vec3f color);

void DrawRect(Vec2i Screen, int x1, int y1, int x2, int y2, Vec3f color);

#endif
