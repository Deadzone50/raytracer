#include <cstdlib>
#include <GL/glut.h>
#include <iostream>
#include "Drawing.hh"
#include "Scene.hh"
#include "RayTracer.hh"

// Display callback -------------------------------------

static Vec2i Screen;
static Scene Sc;

void reshape (int Width, int Height)
{
	glViewport(0, 0, (GLsizei)Width, (GLsizei)Height); // Set viewport to the size of the window
	Screen.y = Height;
	Screen.x = Width;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1.01f, 1.01f, -1.01f, 1.01f, -1.0f, 1.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}  

void display()
{
    
    // flush the drawing to screen . 
    //glutSwapBuffers();
}

// Keyboard callback fu1nction ( called on keyboard event handling )
void keyboard(unsigned char key, int x, int y)
{
    if (key == 'q' || key == 'Q')
        exit(EXIT_SUCCESS);
	else if(key == 'e')
	{
		glClear(GL_COLOR_BUFFER_BIT);   // Erase everything
		RenderOne(Screen, &Sc, x, y, 5);
		glutSwapBuffers();
	}
	else if(key == 'a')
	{
		glClear(GL_COLOR_BUFFER_BIT);   // Erase everything
		int xx, yy;
		std::cin >> xx >> yy;
		RenderOne(Screen, &Sc, xx, yy, 5);
		glutSwapBuffers();
	}
	else if(key == 'w')
	{
		glClear(GL_COLOR_BUFFER_BIT);   // Erase everything
		Render(Screen, &Sc, 10);
		glutSwapBuffers();
	}
	else if(key == 'r')
	{
		glClear(GL_COLOR_BUFFER_BIT);   // Erase everything
		Render(Screen, &Sc, 1);
		glutSwapBuffers();
	}
	else if(key == '1')
	{
		Sc.ParseObjs("Scene.txt");
	}
	else if(key == '2')
	{
		Sc.ParseScene("Scene.txt");
	}
}
void skeyboard(int key, int x, int y)
{
	if(key == GLUT_KEY_LEFT)
	{
		if(glutGetModifiers() == GLUT_ACTIVE_SHIFT)
			Sc.Cam.Turn('l',0.1f);
		else
			Sc.Cam.Move('l',0.1f);
		glClear(GL_COLOR_BUFFER_BIT);   // Erase everything
		Render(Screen, &Sc, 10);
		glutSwapBuffers();
	}
	else if(key == GLUT_KEY_RIGHT)
	{
		if(glutGetModifiers() == GLUT_ACTIVE_SHIFT)
			Sc.Cam.Turn('r',0.1f);
		else
			Sc.Cam.Move('r',0.1f);
		glClear(GL_COLOR_BUFFER_BIT);   // Erase everything
		Render(Screen, &Sc, 10);
		glutSwapBuffers();
	}
	else if(key == GLUT_KEY_UP)
	{
		if(glutGetModifiers() == GLUT_ACTIVE_SHIFT)
			Sc.Cam.Turn('u',0.1f);
		else
			Sc.Cam.Move('u',0.1f);
		glClear(GL_COLOR_BUFFER_BIT);   // Erase everything
		Render(Screen, &Sc, 10);
		glutSwapBuffers();
	}
	else if(key == GLUT_KEY_DOWN)
	{
		if(glutGetModifiers() == GLUT_ACTIVE_SHIFT)
			Sc.Cam.Turn('d',0.1f);
		else
			Sc.Cam.Move('d',0.1f);
		glClear(GL_COLOR_BUFFER_BIT);   // Erase everything
		Render(Screen, &Sc, 10);
		glutSwapBuffers();
	}
	else if(key == GLUT_KEY_PAGE_DOWN)
	{
		if(glutGetModifiers() == GLUT_ACTIVE_SHIFT)
			Sc.Cam.Turn('e',0.1f);
		else
			Sc.Cam.Move('b',0.1f);
		glClear(GL_COLOR_BUFFER_BIT);   // Erase everything
		Render(Screen, &Sc, 10);
		glutSwapBuffers();
	}
	else if(key == GLUT_KEY_PAGE_UP)
	{
		if(glutGetModifiers() == GLUT_ACTIVE_SHIFT)
			Sc.Cam.Turn('q',0.1f);
		else
			Sc.Cam.Move('f',0.1f);
		glClear(GL_COLOR_BUFFER_BIT);   // Erase everything
		Render(Screen, &Sc, 10);
		glutSwapBuffers();
	}
}

// Main execution  function 
int main(int argc, char *argv[])
{
    glutInit(&argc, argv);      // Initialize GLUT
    glutInitDisplayMode (GLUT_DOUBLE); // Set up a basic display (double)buffer 
    //~ glutInitDisplayMode (GLUT_SINGLE); // Set up a basic display (double)buffer 
    glutInitWindowSize (550, 500);
	Screen.x = 550;
    Screen.y = 500;
    glutCreateWindow("OpenGL RayTracer");  // Create a window
    glutDisplayFunc(display);   // Register display callback
    glutIdleFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard); // Register keyboard callback
    glutSpecialFunc(skeyboard); // Register special keyboard callback
    
	Sc.ParseScene("Scene.txt");

	std::cout << "Arrow keys and pgup/down move camera, holding shift rotates instead\n";
	std::cout << "w renders a course image, r renders a fine image, q to quit\n";
	std::cout << "e renders at the mouse position, a expects input {int int} what pixel to render\n";

	glClear(GL_COLOR_BUFFER_BIT);   // Erase everything
	Render(Screen, &Sc, 10);
	glutSwapBuffers();
    
    glutMainLoop();             // Enter main event loop
    return (EXIT_SUCCESS);
}
