#include "RayTracer.hh"
#include "Drawing.hh"
#include <cmath>
#include "Scene.hh"
#include <iostream>
#include <cassert>
#include <omp.h>
#include <ctime>

#define SHADE_BACK 0


Ray::Ray(Vec3f O, Vec3f D)
{
	Origin = O;
	Direction = D;
}

Vec3f Material::GetDiffuseColor(Vec3f P0)
{
	if(!Generated)
		return DiffuseColor;
	else
	{							//checkerboard
		Vec3f P = Scale*P0;
		Vec3i Pt = Vec3i{int(P.x), int(P.y), int(P.z)};
		
		bool Col = 0;

		if(Pt.x%2)
			Col = Col ^ 1;
		if(Pt.y%2)
			Col = Col ^ 1;
		if(Pt.z%2)
			Col = Col ^ 1;
		
		if(P.x + 0.000001 < 0)		//ignore float rounding variations
			Col = Col ^ 1;
		if(P.y + 0.000001 < 0)
			Col = Col ^ 1;
		if(P.z + 0.000001 < 0)
			Col = Col ^ 1;
			
		if(Col)
			return Vec3f{1,1,1};
		else
			return Vec3f{0,0,0};
	}
}
Vec3f Material::GetSpecularColor(Vec3f P)
{
	return SpecularColor;
}

Camera::Camera(Vec3f O, Vec3f F, Vec3f U)
{
	Center = O;
	Forward = Normalize(F);
	Up = Normalize(U);
	Right = Cross(Forward,Up);
	
	D = 1.0f/(tan(Fov/2));		//distance to "image plane", makes the image plane [-1,1]²
	
	std::cout << "CameraC: " << Center  << std::endl;
	std::cout << "CameraF: " << Forward << std::endl;
	std::cout << "CameraR: " << Right << std::endl;
	std::cout << "CameraU: " << Up << std::endl;
	
};

Vec2f GetImageXY(Vec2i XY, Vec2i WH)			//scale to [-1,1]²
{
	float Px = (XY.x - WH.x/2)/((float)WH.x/2);
	float Py = (XY.y - WH.y/2)/((float)WH.y/2);
	Vec2f Cord{Px, Py};
	return Cord;
}

Ray Camera::GenerateRay(Vec2f& Point, float Aspect)		//create ray from camera
{
	Vec3f Dir= ((D*Forward) + (Aspect*Point.x*Right)) + (Point.y*Up);
	//~ std::cout << "RayD: " << Dir << std::endl;
	Dir = Normalize(Dir);
	Ray R(Center, Dir);
	return R;
}

void Camera::Move(char c, float a)
{
	Vec3f Dir;
	if(c == 'l')
	{
		Dir = Cross(Up, Forward);
	}
	else if(c == 'r')
	{
		Dir = Cross(Forward, Up);
	}
	else if(c == 'u')
	{
		Dir = Up;
	}
	else if(c == 'd')
	{
		Dir = -Up;
	}
	else if(c == 'f')
	{
		Dir = Forward;
	}
	else if(c == 'b')
	{
		Dir = -Forward;
	}
	
	Center += (a*Dir);
	//~ std::cout << "CameraC: " << Center  << std::endl;
	//~ std::cout << "CameraF: " << Forward << std::endl;
	//~ std::cout << "CameraR: " << Right << std::endl;
	//~ std::cout << "CameraU: " << Up << std::endl;
}

void Camera::Turn(char c, float a)
{
	Vec3f F = Forward;
	Vec3f U = Up;
	if(c == 'l')
	{
		F += a*Cross(Up, Forward);
		
	}
	else if(c == 'r')
	{
		F -= a*Cross(Up, Forward);
	}
	else if(c == 'u')
	{
		F += a*Up;
		U -= a*Forward;
	}
	else if(c == 'd')
	{
		F -= a*Up;
		U += a*Forward;
	}
	else if(c == 'q')
	{
		U -= a*Right;
	}
	else if(c == 'e')
	{
		U += a*Right;
	}
	Forward = Normalize(F);
	Up = Normalize(U);
	Right = Cross(Forward,Up);
	//~ std::cout << "CameraC: " << Center  << std::endl;
	//~ std::cout << "CameraF: " << Forward << std::endl;
	//~ std::cout << "CameraR: " << Right << std::endl;
	//~ std::cout << "CameraU: " << Up << std::endl;
}

Vec3f MirrorDir(Vec3f Dir, Vec3f N)
{
	Vec3f Mirror = Dir - 2*Dot(Dir, N)*N;
	return Mirror;
}

static clock_t S1;
static clock_t S2;
static clock_t S3;
static clock_t S4;

Vec3f Shade(Ray R0, Hit H, Scene * S, std::vector<float> RefIndex, int Bounces)
{	
	Vec3f P = R0.Origin + H.t*R0.Direction;		//Point of intersection
	Vec3f SC = H.Mat.GetSpecularColor(P);
	Vec3f DC = H.Mat.GetDiffuseColor(P);
	Vec3f AC = Vec3f{0.05f, 0.05f, 0.05f};	//ambient light
	//~ Vec3f AC = Vec3f{0, 0, 0};	//ambient light
	
	Vec3f Specular = Vec3f{0,0,0};
	Vec3f Diffuse = Vec3f{0,0,0};
	Vec3f Ambient = AC*DC;
	
	Vec3f L;					//direction to light
	Vec3f V = -R0.Direction;	//direction to camera
	
	Vec3f Pe = P + EPSILON*V;					//offset the start by a little(backwards)
	
	clock_t t0 = clock();
	for(auto it = S->Lights.begin(); it!=S->Lights.end(); ++it)
	{
		L = it->Center - P;			//toward light
		float DistToLight = Len(L);
		L = Normalize(L);
		
		Ray ShadowRay(Pe, L);
		Hit ShadowHit(DistToLight);
		clock_t t1 = clock();
		Vec3f Shadow = S->IntersectShadow(ShadowRay, ShadowHit);
		S4 += clock() - t1;
		if (Sum(Shadow) > 0)
		{
			Vec3f CLight = it->GetBrightness(DistToLight)*Shadow;		//attunated light
			
			float d = Dot(L, H.Normal);				//diffuse
			if(d < 0)			//clamp to 0
			{
				if(SHADE_BACK == 1)		//shade backside?
				{
					d = -d;				//mirror normal
					Diffuse += d*DC*CLight;
				}
			}
			else
				Diffuse += d*DC*CLight;
			
			Vec3f R = MirrorDir(-L,H.Normal);	//specular
			float s = Dot(R, V);
			if(s > 0)
			{
				s = powf(s, H.Mat.SpecularExponent);
				Specular += s*SC*CLight;
			}
		}
	}
	Vec3f Col = Ambient + (Diffuse + Specular);
	S1 += clock() - t0;
	
	if(Bounces > 0)
	{
		t0 = clock();
		--Bounces;
		if(Sum(H.Mat.ReflectionColor) > 0)
		{
			t0 = clock();
			Vec3f Dir = MirrorDir(-V, H.Normal);
			Ray R = Ray(P + EPSILON*Dir, Dir);		//mirrored ray
			S2 += clock() - t0;
			Col += H.Mat.ReflectionColor*TraceRay(R, S, RefIndex, Bounces);
		}
		
		
		if(Sum(H.Mat.RefractionColor) > 0)
		{
			t0 = clock();
			Vec3f Dir;
			Vec3f N;
			bool error = false;
			float Ir;	//ratio of index'es
			if(Dot(R0.Direction, H.Normal) <= 0)
			{
				Ir = RefIndex.back()/H.Mat.RefractionIndex;	//going into object
				RefIndex.push_back(H.Mat.RefractionIndex);
				N = H.Normal;
			}
			else
			{
				if(RefIndex.size() > 1)
				{
					RefIndex.pop_back();
					Ir = H.Mat.RefractionIndex/RefIndex.back();	//leaving object
					N = -H.Normal;
				}
				else
				{				//error, actually going into object (right at edge)
					error = true;
				}
				
			}
			S3 += clock() - t0;
			if(!error)		//if it just grazes the object ignore refraction
			{
				t0 = clock();
				float SQRT = 1-powf(Ir,2)*(1-powf(Dot(N, V),2));
				if(SQRT >= 0)
				{
					Dir = (Ir*Dot(N, V) - sqrtf(SQRT))*N-Ir*V;
					Dir = Normalize(Dir);
				}
				else
				{
					Dir = MirrorDir(-V, N);		//total internal reflection
				}
				
				Ray R(P+EPSILON*Dir,Dir);
				S3 += clock() - t0;
				Col += H.Mat.RefractionColor*TraceRay(R, S, RefIndex, Bounces);
			}
		}
		
	}
	return Col;
}

static clock_t T1;
static clock_t T2;
#include <time.h>

Vec3f TraceRay(Ray R, Scene *Scene, std::vector<float> RefIndex, int Bounces)
{	
	Hit H(FLT_MAX);
	//~ std::cout << "Ray: ";
	clock_t t0 = clock();
	bool hit = Scene->Intersect(R, H);
	T1 += clock() - t0;

	if (hit)
	{
		t0 = clock();
		Vec3f Col = Shade(R, H, Scene, RefIndex, Bounces);
		T2 += clock() - t0;
		return Col;
		
	}
	else
		return Vec3f{0, 0, 0};
}

extern clock_t SPC;
extern clock_t BOC;

void Render(Vec2i Screen, Scene *Scene, int DS)
{
	int const Width = Screen.x;
	int const Height = Screen.y;
	float const Aspect = (float)Width/Height;
	int const Bounces = 5;
	SPC = 0;
	BOC = 0;
	std::vector<float> RefIndex;		//stack of Refractionindex'es
	RefIndex.push_back(1);				//air//TODO: check if inside object at start
	Vec3f *Image = new Vec3f[Height*Width];

	clock_t t0 = clock();

	#pragma omp parallel for
	for(int y = 0; y < Height; y+=DS)			//calculate screen multithreaded
	{
		for(int x = 0; x < Width; x+=DS)
		{
			Vec2f XY = GetImageXY(Vec2i{x+DS/2, y+DS/2}, Screen);
			Ray R = Scene->Cam.GenerateRay(XY, Aspect);
			Vec3f Color = TraceRay(R, Scene, RefIndex, Bounces);
			Image[y*Width + x] = Color;
		}
	}
	std::cout << "Total time: " << (float)(clock()-t0)/CLOCKS_PER_SEC << std::endl;

	std::cout << "Intersecting: " << (float)T1/CLOCKS_PER_SEC << std::endl;
	std::cout << "Shading: " << (float)T2/CLOCKS_PER_SEC << std::endl;
	std::cout << "Shading1: " << (float)S1/CLOCKS_PER_SEC << std::endl;
	std::cout << "Shading2: " << (float)S2/CLOCKS_PER_SEC << std::endl;
	std::cout << "Shading3: " << (float)S3/CLOCKS_PER_SEC << std::endl;
	std::cout << "Shadow: " << (float)S4/CLOCKS_PER_SEC << std::endl;
	std::cout << "SP: " << (float)SPC/CLOCKS_PER_SEC << std::endl;
	std::cout << "BO: " << (float)BOC/CLOCKS_PER_SEC << std::endl;
	T1 = 0;
	T2 = 0;
	S1 = 0;
	S2 = 0;
	S3 = 0;
	S4 = 0;
	SPC = 0;
	BOC = 0;
	
	
	for(int y = 0; y < Height; y+=DS)			//draw sceen with single thread
	{
		for(int x = 0; x < Width; x+=DS)
		{
			if(DS > 1)
				DrawRect(Screen, x, y, x+DS, y+DS, Image[y*Width + x]);
			else
				DrawPoint(Screen, x, y, Image[y*Width + x]);
		}
	}
}

void RenderOne(Vec2i Screen, Scene *Scene, int x, int yy, int Size)		//for debugging
{
	int Width = Screen.x;
	int Height = Screen.y;
	float Aspect = Width/Height;
	int Bounces = 3;
	std::vector<float> RefIndex;		//stack of Refractionindex'es
	RefIndex.push_back(1);				//air//TODO: check if inside object at start
	
	int y = Height- yy;
		
	Vec2f XY = GetImageXY(Vec2i{x, y}, Screen);
	std::cout << XY.x << " : " << XY.y << std::endl;
	std::cout << x << " : " << yy << std::endl;
	Ray R = Scene->Cam.GenerateRay(XY, Aspect);
	Vec3f Color = TraceRay(R, Scene, RefIndex, Bounces);

	if(Size > 1)
		DrawRect(Screen, x-Size/2, y-Size/2, x+Size/2, y+Size/2, Color);
	else
		DrawPoint(Screen, x, y, Color);
}
