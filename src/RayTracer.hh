#ifndef RAYTRACER_HH
#define RAYTRACER_HH

#include <vector>
#include "Drawing.hh"
#include "Vector.hh"


class Scene;

class Material
{
	public:
	Vec3f GetDiffuseColor(Vec3f P);
	Vec3f GetSpecularColor(Vec3f P);
	float SpecularExponent = 1;
	float RefractionIndex = 1;
	Vec3f ReflectionColor = Vec3f{0,0,0};
	Vec3f RefractionColor = Vec3f{0,0,0};
	bool Generated = false;
	float Scale = 1;
	Vec3f SpecularColor = Vec3f{0,0,0};
	Vec3f DiffuseColor = Vec3f{0,0,0};
};

class Hit
{
public:
	Hit(float t) : t(t) {};
	Material Mat;
	Vec3f Normal;
	float t;
};

class Ray
{
	public:
	Ray(Vec3f Origen, Vec3f Dir);
	Vec3f Origin;
	Vec3f Direction;
};


class Camera
{
	public:
		Camera() {};
	Camera(Vec3f Center, Vec3f Forward, Vec3f Up);
	void Move(char c, float a);
	void Turn(char c, float a);
	Ray GenerateRay(Vec2f& Point, float AspectRatio);
	Vec3f Center;
	Vec3f Right;
	Vec3f Up;
	Vec3f Forward;
	
	float D;
	float Fov = 1;
};

Vec3f TraceRay(Ray R, Scene *Scene, std::vector<float> RefIndex, int Bounces);
Vec3f MirrorDir(Vec3f Dir, Vec3f Normal);

void Render(Vec2i Screen, Scene *Scene, int DownSample);
void RenderOne(Vec2i Screen, Scene *Scene, int x, int y, int Size);

#endif
