#include "Scene.hh"
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <cmath>
#include <ctime>

clock_t SPC;
clock_t BOC;

float Light::GetBrightness(float Dist)
{
	return Brightness;
}

void Scene::ParseObjs(std::string FileName)
{
	std::ifstream file;
	file.open(FileName);
	if(!file.is_open())
		return;
	std::string S;
	bool Run = true;

	Vec3f V1, V2, V3;
	float F1;
	int I1;
	std::vector<Material> Materials;
	World.clear();
	Lights.clear();
	while(Run)
	{
		file >> S;
		if(!S.compare("Light"))
		{
			std::cout << "Light\n";
			file >> V1.x >> V1.y >> V1.z;
			file >> V2.x >> V2.y >> V2.z;
			file >> V3.x >> V3.y >> V3.z;
			Lights.push_back(Light(V1,V2,V3));
		}
		else if(!S.compare("<M>"))
		{
			Material Mat;
			std::cout << "Material\n";
			while(S.compare("</M>"))
			{
				file >> S;
				if(!S.compare("Diffuse"))
				{
					file >> V1.x >> V1.y >> V1.z;
					Mat.DiffuseColor = V1;
				}
				else if(!S.compare("Specular"))
				{
					file >> V1.x >> V1.y >> V1.z;
					Mat.SpecularColor = V1;
				}
				else if(!S.compare("SpecularExponent"))
				{
					file >> F1;
					Mat.SpecularExponent = F1;
				}
				else if(!S.compare("Reflection"))
				{
					file >> V1.x >> V1.y >> V1.z;
					Mat.ReflectionColor = V1;
				}
				else if(!S.compare("Refraction"))
				{
					file >> V1.x >> V1.y >> V1.z;
					Mat.RefractionColor = V1;
				}
				else if(!S.compare("RefractionIndex"))
				{
					file >> F1;
					Mat.RefractionIndex = F1;
				}
				else if(!S.compare("Generated"))
				{
					Mat.Generated = true;
				}
				else if(!S.compare("Scale"))
				{
					file >> F1;
					Mat.Scale = F1;
				}
			}
			Materials.push_back(Mat);
		}
		else if(!S.compare("Triangle"))
		{
			std::cout << "Triangle\n";
			file >> V1.x >> V1.y >> V1.z;
			file >> V2.x >> V2.y >> V2.z;
			file >> V3.x >> V3.y >> V3.z;
			Triangle *TO = new Triangle(V1,V2,V3);
			World.push_back(TO);
		}
		else if(!S.compare("<Sphere>"))
		{
			std::cout << "Sphere\n";
			while(S.compare("</Sphere>"))
			{
				file >> S;
				if(!S.compare("Center"))
				{
					file >> V1.x >> V1.y >> V1.z;
				}
				else if(!S.compare("Radius"))
				{
					file >> F1;
				}
				else if(!S.compare("Material"))
				{
					file >> I1;
				}
			}
			Sphere *SO = new Sphere(V1,F1, Materials[I1]);
			World.push_back(SO);
		}
		else if(!S.compare("<Box>"))
		{
			std::cout << "Box\n";
			while(S.compare("</Box>"))
			{
				file >> S;
				if(!S.compare("Min"))
				{
					file >> V1.x >> V1.y >> V1.z;
				}
				else if(!S.compare("Max"))
				{
					file >> V2.x >> V2.y >> V2.z;
				}
				else if(!S.compare("Material"))
				{
					file >> I1;
				}
			}
			Box *BO = new Box(V1,V2, Materials[I1]);
			World.push_back(BO);
		}
		else if(!S.compare("<Plane>"))
		{
			std::cout << "Plane\n";
			while(S.compare("</Plane>"))
			{
				file >> S;
				if(!S.compare("Normal"))
				{
					file >> V1.x >> V1.y >> V1.z;
				}
				else if(!S.compare("Distance"))
				{
					file >> F1;
				}
				else if(!S.compare("Material"))
				{
					file >> I1;
				}
			}
			Plane *PO = new Plane(V1, F1, Materials[I1]);
			World.push_back(PO);
		}
		else if(!S.compare("End"))
		{
			Run = false;
		}
	}
	file.close();
}

void Scene::ParseScene(std::string FileName)
{
	std::ifstream file;
	file.open(FileName);
	if(!file.is_open())
		return;
	std::string S;
	bool Run = true;

	Vec3f V1, V2, V3;
	std::vector<Material> Materials;
	while(Run)
	{
		file >> S;
		if(!S.compare("Camera"))
		{
			std::cout << "Camera\n";
			file >> V1.x >> V1.y >> V1.z;
			file >> V2.x >> V2.y >> V2.z;
			file >> V3.x >> V3.y >> V3.z;
			Cam = Camera(V1,V2,V3);
		}
		else if(!S.compare("End"))
		{
			Run = false;
		}
	}
	file.close();
	ParseObjs(FileName);
}

Light::Light(Vec3f Pos, Vec3f Dir, Vec3f C)
{
	Center = Pos;
	Direction = Normalize(Dir);
	Color = C;
}

Vec3f Scene::IntersectShadow(Ray R, Hit &H0)		//with "transparent shadows"
{
	Vec3f attunation{1,1,1};
	for(auto It = World.begin(); It != World.end(); ++It)
	{
		Hit H1(H0.t);
		Hit H2(H0.t);
		if((*It)->Intersect(R,H1))
		{
			if(Dot(H1.Mat.RefractionColor, Vec3f{1,1,1}) > 0)	//if transparent, use second ray to find lenght through object
			{
				float t1 = H1.t;												//TODO: fix for rays starting in objects
				Ray R2(R.Origin + (t1+EPSILON)*R.Direction, R.Direction);		//starts at the first hit
				(*It)->Intersect(R2,H2);
				float t2 = H2.t;
				float a = 1.0f/(5*powf(t2,2));
				if(a > 1)
					a = 1;
				attunation *= a*H1.Mat.RefractionColor;
				
			}
			else
			{
				return Vec3f{0,0,0};		//solid object
			}
		}
	}
	return attunation;
}
//~ Vec3f Scene::IntersectShadow(Ray R, Hit &H0)		//simple
//~ {
	//~ for(auto It = World.begin(); It != World.end(); ++It)
	//~ {
		//~ if((*It)->Intersect(R,H0))
		//~ {
			//~ return Vec3f{0,0,0};		//solid object
		//~ }
	//~ }
	//~ return Vec3f{1,1,1};
//~ }

bool Scene::Intersect(Ray R, Hit &H)				//TODO: smarter lookup
{
	bool Intersect = false;
	for(auto It = World.begin(); It != World.end(); ++It)
	{
		if((*It)->Intersect(R,H))
		{
			Intersect = true;
		}
	}
	return Intersect;
}

bool SameSide(Vec3f P1, Vec3f P2, Vec3f A, Vec3f B)
{
	Vec3f CP1 = Cross(B-A, P1-A);
	Vec3f CP2 = Cross(B-A, P2-A);
	if (Dot(CP1, CP2) >= 0)
		return true;
	else
		return false;
}

bool TriangleTest(Vec3f P, Vec3f A, Vec3f B, Vec3f C)
{
	if (SameSide(P, A, B, C) && SameSide(P, B, A, C) && SameSide(P, C, A, B))
		return true;
	else
		return false;
}

Triangle::Triangle(Vec3f v1, Vec3f v2, Vec3f v3)
{
	V1 = v1;
	V2 = v2;
	V3 = v3;
	Normal = Cross(V2-V1,V3-V2);
}

bool Triangle::Intersect(Ray R, Hit &H)
{
	Vec3f R0 = R.Origin;
	Vec3f Rd = R.Direction;
	float D = -Dot(Normal, V1);
	float NRd = Dot(Normal, Rd);
	if(NRd != 0)				//not parallel
	{
		float t = -(D+Dot(Normal, R0))/(NRd);
		if(t < H.t && t >= 0)
		{
			Vec3f P = R0 + (t*Rd);
			if(TriangleTest(P,V1,V2,V3))
			{
				H.t = t;
				H.Mat = Mat;
				H.Normal = Normal;
				//~ std::cout << "hit: "<< P << std::endl;
				return true;
			}
		}
	}
	//~ std::cout << "miss" << std::endl;
	return false;
}

Sphere::Sphere(Vec3f C, float R, Material M)
{
	Center = C;
	Radius = R;
	Mat = M;
}

bool Sphere::Intersect(Ray R, Hit &H)
{
	clock_t tc = clock();
	//Vec3f P = R.Origin+R.Direction*t;
	
	//(P-Center)² = (Radius)²
	Vec3f OC = R.Origin - Center;
	Vec3f Dir = R.Direction;
	
	//quadratic equation coefficients
	float a = 1;		//assuming direction of ray is normalized
	float b = 2*Dot(OC,Dir);
	float c = Dot(OC,OC) - (Radius*Radius);
	
	//discriminant
	float D = b*b - 4*a*c;
	if(D<0)			//0 roots
	{
		//~ std::cout << "miss" << std::endl;
		SPC += clock() - tc;
		return false;
	}
	else if(D>0)	//2 roots
	{
		float t0 = (-b -sqrtf(D))/(2*a);	//always smaller
		float t1 = (-b +sqrtf(D))/(2*a);	//always bigger
		float t;
		
		if(t0 < H.t && t0 >= 0)
			t = t0;
		else if(t1 < H.t && t1 >= 0)
			t = t1;
		else
		{
			//~ std::cout << "miss" << std::endl;
			SPC += clock() - tc;
			return false;
		}
		
		Vec3f P = R.Origin+t*R.Direction;
		H.t = t;
		H.Mat = Mat;
		H.Normal = Normalize(P-Center);
		//~ std::cout << "hit: " << P << std::endl;
		SPC += clock() - tc;
		return true;
	}
	else		//1 root
	{
		float t = -b/(2*a);
		if(t < H.t && t >= 0)
		{
			Vec3f P = R.Origin+t*R.Direction;
			H.t = t;
			H.Mat = Mat;
			H.Normal = Normalize(P-Center);
			//~ std::cout << " hit: " << P << std::endl;
			SPC += clock() - tc;
			return true;
		}
		//~ std::cout << "miss" << std::endl;
		SPC += clock() - tc;
		return false;
	}
}

Plane::Plane(Vec3f N, float D, Material M)
{
	Normal = N;
	D = D;
	P0 = D*N;		//point on plane
	Mat = M;
}

bool Plane::Intersect(Ray R, Hit &H)
{
	
	//~ (P - P0) dot N = 0 
	//~ ((Ro + t*Rd) - P0) dot N = 0
	//~ (Ro-P0).N + t*Rd.N = 0
	//~ t=(P0 - Ro).N / Rd.N
	
	float Div = Dot(R.Direction,Normal);
	if(Div == 0)		// parallel
	{
		//~ std::cout << "miss" << std::endl;
		return false;
	}
	else
	{
		float t = Dot((P0 - R.Origin),Normal)/Div;
		
		if((t >= 0)&&(t < H.t))
		{
			H.t = t;
			H.Normal = Normal;
			H.Mat = Mat;
			//~ Vec3f P = R.Origin + t*R.Direction;
			//~ std::cout << "hit: " << P << std::endl;
			return true;
		}
		
		//~ std::cout << "miss" << std::endl;
		return false;
	}
}

Box::Box(Vec3f Min, Vec3f Max, Material M)
{
	V1 = Min;
	V2 = Max;
	Mat = M;
}

bool Box::Intersect(Ray R, Hit &H)		//TODO: make faster
{
	clock_t tc = clock();
	Vec3f Nx = Vec3f{1 , 0, 0};		//normals
	Vec3f Ny = Vec3f{0 , 1, 0};
	Vec3f Nz = Vec3f{0 , 0, 1};
	
	//~ (Ro + t*Rd - P).N = 0
	//~ t=(P0 - Ro).N / Rd.N
	float t0 = -FLT_MAX, t1 = FLT_MAX;		//original segment
	Vec3f t0N, t1N;							//normals for the points
	float t0x, t1x;
	float t0y, t1y;
	float t0z, t1z;
	
	float Div = Dot(R.Direction, Nx);
	if(Div != 0)		//not parallel
	{
		t0x = Dot((V2 - R.Origin),Nx)/Div;		//where on the x end-planes it intersects
		t1x = -Dot((V1 - R.Origin),-Nx)/Div;
	}
	else
	{														//parallel
		if((R.Origin.x >= V1.x) &&(R.Origin.x <= V2.x))		//check bounds
		{
			t0x = -FLT_MAX;
			t1x = FLT_MAX;
		}
		else
		{
			BOC += clock() - tc;
			return false;
		}
		
	}
	
	Div = Dot(R.Direction, Ny);
	if(Div != 0)		//not parallel
	{
		t0y = Dot((V2 - R.Origin),Ny)/Div;		//where on the y end-planes it intersects
		t1y = -Dot((V1 - R.Origin),-Ny)/Div;
	}
	else
	{
		if((R.Origin.y >= V1.y)&&(R.Origin.y <= V2.y))		//check bounds
		{
			t0y = -FLT_MAX;
			t1y = FLT_MAX;
		}
		else
		{
			BOC += clock() - tc;
			return false;
		}
	}
	Div = Dot(R.Direction, Nz);
	if(Div != 0)		//not parallel
	{
		t0z = Dot((V2 - R.Origin),Nz)/Div;		//where on the z end-planes it intersects
		t1z = -Dot((V1 - R.Origin),-Nz)/Div;
	}
	else
	{
		if((R.Origin.z >= V1.z) &&(R.Origin.z <= V2.z))		//check bounds
		{
			t0z = -FLT_MAX;
			t1z = FLT_MAX;
		}
		else
		{
			BOC += clock() - tc;
			return false;
		}
	}
	
	Vec2f MM;
	Vec2V MMN;		//keep track of min/max t's and normals
	if(t0x < t1x)
	{
		MM = Vec2f{t0x, t1x};
		MMN = Vec2V{Nx, -Nx};
	}
	else
	{
		MM = Vec2f{t1x, t0x};
		MMN = Vec2V{-Nx, Nx};
	}
	if(MM.x > t0)			//start shrinking the line segment
	{
		t0 = MM.x;
		t0N = MMN.x;
	}
	if(MM.y < t1)
	{
		t1 = MM.y;
		t1N = MMN.y;
	}

	if(t0y < t1y)
	{
		MM = Vec2f{t0y, t1y};
		MMN = Vec2V{Ny, -Ny};
	}
	else
	{
		MM = Vec2f{t1y, t0y};
		MMN = Vec2V{-Ny, Ny};
	}
	
	if(MM.x > t0)
	{
		t0 = MM.x;
		t0N = MMN.x;
	}
	if(MM.y < t1)
	{
		t1 = MM.y;
		t1N = MMN.y;
	}
	
	if(t0z < t1z)
	{
		MM = Vec2f{t0z, t1z};
		MMN = Vec2V{Nz, -Nz};
	}
	else
	{
		MM = Vec2f{t1z, t0z};
		MMN = Vec2V{-Nz, Nz};
	}
	
	if(MM.x > t0)
	{
		t0 = MM.x;
		t0N = MMN.x;
	}
	if(MM.y < t1)
	{
		t1 = MM.y;
		t1N = MMN.y;
	}
	
	float t;
	Vec3f Normal;
	if(t0 >= t1)
	{
		BOC += clock() - tc;
		return false;
	}
	
	if(t0 >= 0)		//always smaller than t1
	{
		t = t0;
		Normal = t0N;
	}
	else if(t1 >= 0)
	{
		t = t1;
		Normal = t1N;
	}
	else
	{
		BOC += clock() - tc;
		return false;		//behind camera
	}

	if(t < H.t)
	{
		H.t = t;
		H.Normal = Normal;
		H.Mat = Mat;
		BOC += clock() - tc;
		return true;
	}
	BOC += clock() - tc;
	return false;
	
}

Scene::~Scene()
{
	for(auto It = World.begin(); It!=World.end();++It)
	{
		delete *It;
	}
}
