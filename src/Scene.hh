#ifndef SCENE_HH
#define SCENE_HH

#include <vector>
#include "RayTracer.hh"
#include "Vector.hh"
#include <string>
#include "Drawing.hh"



class Obj
{
public:
	Obj(){};
	virtual bool Intersect(Ray R, Hit &H) = 0;
	Material Mat;
	virtual ~Obj(){};
};

class Triangle: public Obj
{
	public:
	Triangle(Vec3f V1, Vec3f V2, Vec3f V3);
	bool Intersect(Ray R, Hit &H) override;
	Vec3f V1, V2, V3, Normal;
	~Triangle(){};
};

class Sphere: public Obj
{
	public:
	Sphere(Vec3f C, float R, Material M);
	bool Intersect(Ray R, Hit &H) override;
	Vec3f Center;
	float Radius;
	~Sphere(){};
};

class Box: public Obj
{
	public:
	Box(Vec3f Min, Vec3f Max, Material M);
	bool Intersect(Ray R, Hit &H) override;
	Vec3f V1, V2;
	~Box(){};
};

class Plane: public Obj
{
	public:
	Plane(Vec3f N, float D, Material M);
	bool Intersect(Ray R, Hit &H) override;
	Vec3f Normal, P0;
	float D;
	~Plane(){};
};

class Light
{
	public:
	Light(Vec3f Pos, Vec3f Dir, Vec3f C);
	float GetBrightness(float Dist);
	
	Vec3f Center, Direction, Color;
	float Brightness = 1;
	float QAttenuation = 1;	//quaddratic
	float LAttenuation = 1;	//linear
	float CAttenuation = 1;	//constant
};

class Scene
{
public:
	void ParseScene(std::string FileName);
	void ParseObjs(std::string FileName);
	bool Intersect(Ray R, Hit &H);
	Vec3f IntersectShadow(Ray R, Hit &H0);
	
	~Scene();

	Camera Cam;
	std::vector<Light> Lights;
	std::vector<Obj*> World;
};


#endif
