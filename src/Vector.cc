#include "Vector.hh"

#include <cmath>
#include <string>
#include <sstream>

Vec2f MinMax(float f1, float f2)
{
	if(f1 < f2)
		return Vec2f{f1,f2};
	else
		return Vec2f{f2,f1};
}

Vec3f Normalize(Vec3f V)
{
	float l = sqrt(V.x*V.x + V.y*V.y + V.z*V.z);
	Vec3f VN ={V.x/l, V.y/l, V.z/l};
	return VN;
}

float Len(Vec3f V)
{
	return sqrt(V.x*V.x + V.y*V.y + V.z*V.z);
}
float Sum(Vec3f V)
{
	return V.x+V.y+V.z;
}

Vec3f Cross(Vec3f B, Vec3f A)
{
	Vec3f C{A.y*B.z-A.z*B.y, A.z*B.x-A.x*B.z, A.x*B.y-A.y*B.x};
	return C;
}
float Dot(Vec3f V1, Vec3f V2)
{
	float f = V1.x*V2.x + V1.y*V2.y + V1.z*V2.z;
	return f;
}

Vec3f operator*(float f, const Vec3f &V1)
{
	Vec3f V;
	V.x=f*V1.x;
	V.y=f*V1.y;
	V.z=f*V1.z;
	return V;
}
Vec3i operator*(float f, const Vec3i &V1)
{
	Vec3i V;
	V.x=f*V1.x;
	V.y=f*V1.y;
	V.z=f*V1.z;
	return V;
}
Vec3f operator*(const Vec3f &V1, const Vec3f &V2)
{
	Vec3f V;
	V.x = V1.x*V2.x;
	V.y = V1.y*V2.y;
	V.z = V1.z*V2.z;
	return V;
}
void operator*=(Vec3f &V1, const Vec3f &V2)
{
	V1.x *= V2.x;
	V1.y *= V2.y;
	V1.z *= V2.z;
}
void operator+=(Vec3f &V1, const Vec3f &V2)
{
	V1.x += V2.x;
	V1.y += V2.y;
	V1.z += V2.z;
}
void operator-=(Vec3f &V1, const Vec3f &V2)
{
	V1.x -= V2.x;
	V1.y -= V2.y;
	V1.z -= V2.z;
}
Vec3f operator+(const Vec3f &V1, const Vec3f &V2)
{
	Vec3f V;
	V.x = V1.x+V2.x;
	V.y = V1.y+V2.y;
	V.z = V1.z+V2.z;
	return V;
}

Vec3f operator-(const Vec3f &V1, const Vec3f &V2)
{
	Vec3f V;
	V.x = V1.x-V2.x;
	V.y = V1.y-V2.y;
	V.z = V1.z-V2.z;
	return V;
}
Vec3f operator-(const Vec3f &V1)
{
	Vec3f V;
	V.x = -V1.x;
	V.y = -V1.y;
	V.z = -V1.z;
	return V;
}

std::ostream& operator<<(std::ostream& os, const Vec3f& V)
{
  os << V.x << "," << V.y << "," << V.z;
  return os;
}
