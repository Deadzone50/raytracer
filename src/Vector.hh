#ifndef VECTOR_HH
#define VECTOR_HH

#define EPSILON 0.0001f
#include <sstream>

//#define FLT_MAX 1e30
typedef struct
{
	float x;
	float y;
	float z;
} Vec3f;

typedef struct
{
	float x;
	float y;
} Vec2f;

typedef struct
{
	Vec3f x;
	Vec3f y;
} Vec2V;

typedef struct
{
	int x;
	int y;
	int z;
} Vec3i;

typedef struct
{
	int x;
	int y;
} Vec2i;

Vec3f Normalize(Vec3f V);
Vec3f Cross(Vec3f A, Vec3f B);
float Dot(Vec3f V1, Vec3f V2);
float Sum(Vec3f V);
float Len(Vec3f V);
Vec2f MinMax(float f1, float f2);

Vec3f operator*(float f, const Vec3f &V);
Vec3i operator*(float f, const Vec3i &V);
Vec3f operator*(const Vec3f &V1, const Vec3f &V2);
Vec3f operator+(const Vec3f &V1, const Vec3f &V2);
void  operator+=(Vec3f &V1, const Vec3f &V2);
void  operator-=(Vec3f &V1, const Vec3f &V2);
void  operator*=(Vec3f &V1, const Vec3f &V2);
Vec3f operator-(const Vec3f &V1, const Vec3f &V2);
Vec3f operator-(const Vec3f &V1);
std::ostream& operator<<(std::ostream& os, const Vec3f& V);


#endif
